const nom = 'Philippe Fleury'
const sexe = 'M'
const dateNaissance = new Date(1985, 11, 15)
const situationParticuliere = false

let nbJours = prompt('Sur combien de jours voulez-vous faire votre analyse')
nbJours = parseInt(nbJours)
console.log("Nombre de jours analysés: "+ nbJours)

const nbConsommationAlcool = []
let sommeConsomation = 0
for (let i = 1; i <= nbJours; i++) {
  let journeesAlcool = prompt('Consomation journalière ' + i)
  journeesAlcool = parseInt(journeesAlcool)
  sommeConsomation += journeesAlcool
  nbConsommationAlcool.push(journeesAlcool)

}

function verificationLimite(sexe, situationParticuliere) {
    let maxAlcoolSemaine = 0
let maxAlcoolJour = 0

if (situationParticuliere == true ) {
    maxAlcoolSemaine = 0
    maxAlcoolJour = 0
} else if (sexe === 'M') {
    maxAlcoolSemaine = 15
    maxAlcoolJour = 3

    }else {
        maxAlcoolSemaine = 10
        maxAlcoolJour = 2
    }
    return {maxAlcoolSemaine:maxAlcoolSemaine, maxAlcoolJour:maxAlcoolJour}
}
const limites = verificationLimite(sexe, situationParticuliere)

let recommandationAlcoolRespectee = true


let date = new Date()

console.log("Date d'aujourd'hui: "+ date.toLocaleDateString('fr-ca'))
console.log(nom)



function ageFunction() { 
     const dateActuelle = new Date()
     const nbMillisecondes = dateActuelle - dateNaissance
     let age = nbMillisecondes / (1000 * 60 * 60 * 24 * 365.25)
     age = Math.floor(age)
     return age
 }
let age = ageFunction()

console.log("age: "+ age)
console.log("Alcool")


let moyenneJournaliere = 0

moyenneJournaliere = sommeConsomation/nbJours
moyenneJournaliere = Math.floor(moyenneJournaliere)
console.log("Consomation d'alcool moyenne journalière: "+ moyenneJournaliere )


let moyenneHebdo=0

let nbSemaine= nbJours/7
console.log("Nombre de semaines examinee: "+ nbSemaine)
moyenneHebdo= sommeConsomation/nbSemaine
console.log("Consomation d'alcool moyenne par semaine: " + moyenneHebdo )
//si le nombre de jours analyse<1, il invente les jours restant selon la moyenne obtenue


if (moyenneHebdo > limites.maxAlcoolSemaine) {
    recommandationAlcoolRespectee=false
}

const max = Math.max(...nbConsommationAlcool)

if (max > limites.maxAlcoolJour) {
    recommandationAlcoolRespectee = false
    
}
console.log('max:'+ max)


let nbJoursExcedants = 0
let nbJoursZero = 0
for(let i=0; i<nbConsommationAlcool.length; i++) {
    if (nbConsommationAlcool[i]>limites.maxAlcoolJour) {
        nbJoursExcedants++
    } else if (nbConsommationAlcool[i]==0){
        nbJoursZero++
    }
}
console.log('Nombres de jours excédants: '+ nbJoursExcedants)

const ratioExcedant = nbJoursExcedants/nbJours
console.log('Ratio des jours excédants: '+ Math.round(ratioExcedant*100)/100)

const ratioZero = nbJoursZero/nbJours
console.log('Ratio des jours sans consomations: '+ Math.round(ratioZero*100)/100)

if (recommandationAlcoolRespectee) {
    console.log('Vous respectez les recommandations') 
    
}else {
    console.log('Vous ne respectez pas les recommandations')
}



